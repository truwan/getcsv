package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"github.com/truwan/getcsv"
)

var port = 3000

func parseInt(str []string) int {
	res, _ := strconv.ParseInt(str[0], 10, 0)
	return int(res)
}

// return requested date as a timestamp
func parseDate(str []string) string {
	var res string
	if len(str[0]) == 0 {
		res = "2018-01-01T00:00" // default date
	} else {
		res = str[0]
	}
	resStrings := strings.Split(res, "T")

	return resStrings[0] + " " + resStrings[1] + ":00"
}

func handler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	vars := r.Form
	fmt.Print(vars)

	params := getcsv.Parameters{
		Done:  parseInt(vars["DoneBut"]),
		SDate: parseDate(vars["SDate"]),
		EDate: parseDate(vars["EDate"]),
	}

	//fmt.Println(params)

	if getcsv.Request(params) == 1 {
		w.Header().Set("Content-Disposition", "attachment; filename=result.csv")
		w.Header().Set("Content-Type", r.Header.Get("Content-Type"))
		http.ServeFile(w, r, "result.csv")
	} else {
		// no data found
		http.NotFound(w, r)
	}

}

func main() {
	//fmt.Println("hello from server")

	router := mux.NewRouter()
	router.HandleFunc("/get", func(w http.ResponseWriter, r *http.Request) { handler(w, r) })
	router.PathPrefix("/").Handler(http.FileServer(http.Dir("web")))

	addr := fmt.Sprintf(":%d", port)
	log.Print("Starting the server on port", addr)
	log.Fatal(http.ListenAndServe(addr, router))

}
