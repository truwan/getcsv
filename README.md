# GetCSV
> Trubetskov Ivan


Getting a csv file containing orders's data from the PostgreSQL table using web interface



The table to house the data is supposed to be created:
```
create table ORDERS(
    Num	 serial CONSTRAINT pk_orders PRIMARY KEY,
    OrderDate timestamp NOT NULL,
    check(OrderDate <= current_timestamp),
    Total numeric(10,2) NOT NULL DEFAULT 0,
    check(total >= 0),
    Done numeric(1) NOT NULL DEFAULT 0,
    check(done = 0 or done = 1),
    DoneDate timestamp, 
    check(DoneDate <= current_timestamp AND DoneDate >=OrderDate)
);
```


If no data was found then 404 Not Found will be returned, otherwise
a csv file containing the requested data called 'result.csv' will be downloaded.


### Launching
``` go run server/main.go ```

Then visit http://localhost:3000/ in your browser

### Dependencies
* Go
* PostgreSQL
