package getcsv

import (
	"database/sql"
	"encoding/csv"
	"fmt"
	"os"

	_ "github.com/lib/pq"
)

const (
	dbUser     = 
	dbPassword = 
	dbName     = 
	tableName  = "orders"
)

var (
	num, orderDate, total, done string
	doneDate                    sql.NullString
)

// Parameters structure contains request parameters
type Parameters struct {
	Done  int
	SDate string
	EDate string
}

// error handling
func checkErr(err error, message string) {
	if err != nil {
		fmt.Println(message)
		panic(err)
	}
}

// write to a csv file
func writeToCsv(writer *csv.Writer, rows *sql.Rows) {
	err := rows.Scan(&num, &orderDate, &total, &done, &doneDate)
	checkErr(err, "Can't Scan")

	if doneDate.Valid {
		err = writer.Write([]string{num, orderDate, total, done, doneDate.String})
		checkErr(err, "Can't write")
	} else {
		err = writer.Write([]string{num, orderDate, total, done})
		checkErr(err, "Can't write")
	}
}

// Request to the database based on Parameters
func Request(params Parameters) int {
	//fmt.Println("hi")
	dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable", dbUser, dbPassword, dbName)
	db, err := sql.Open("postgres", dbinfo)
	checkErr(err, "Can't connect")
	db.SetMaxOpenConns(16) //
	db.SetMaxIdleConns(16) //
	defer db.Close()

	var query string
	var rows *sql.Rows

	if params.Done == 2 { // all
		query = "SELECT num,OrderDate,total,Done,DoneDate FROM %s WHERE OrderDate BETWEEN $1::timestamp AND $2::timestamp"
		query = fmt.Sprintf(query, tableName)
		rows, err = db.Query(query, params.SDate, params.EDate)
		checkErr(err, "Query error")
	} else {
		query = "SELECT num,OrderDate,total,Done,DoneDate FROM %s WHERE Done = $1 AND OrderDate BETWEEN $2::timestamp AND $3::timestamp"
		query = fmt.Sprintf(query, tableName)
		rows, err = db.Query(query, params.Done, params.SDate, params.EDate)
		checkErr(err, "Query error")
	}

	defer rows.Close()

	if !rows.Next() {
		//fmt.Println("NO ROWS")
		return 0
	}

	file, err := os.Create("result.csv")
	checkErr(err, "Can't create")
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	// because of !rows.Next()
	writeToCsv(writer, rows)

	for rows.Next() {
		writeToCsv(writer, rows)
	}

	return 1
}
